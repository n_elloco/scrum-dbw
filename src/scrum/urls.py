from django.conf.urls import patterns, include, url
from django.views.generic import TemplateView
from rest_framework.authtoken.views import obtain_auth_token

from board.urls import board_router

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'scrum.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^api/token/', obtain_auth_token, name='api-token'),
    url(r'^api/', include(board_router.urls), name='api-root'),
    url(r'^$', TemplateView.as_view(template_name='board/index.html')),
)
