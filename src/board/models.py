# coding: utf-8
from django.conf import settings
from django.db import models


class TaskState(object):
    """ Enumerate of task states
    """
    TODO = 1
    PROGRESS = 2
    TESTING = 3
    DONE = 4

    values = {
        TODO: 'Не начато',
        PROGRESS: 'В работе',
        TESTING: 'В тестировании',
        DONE: 'Выполнено'
    }

    @classmethod
    def get_choices(cls):
        return tuple(cls.values.items())


class Sprint(models.Model):
    """ Sprint model
    """
    name = models.CharField(max_length=100, default='', blank=True)
    description = models.TextField(default='', blank=True)
    end = models.DateField(unique=True)

    def __str__(self):
        return self.name or 'Sprint ending %s' % self.end

    class Meta:
        db_table = 'sprint'


class Task(models.Model):
    """ Task model
    """
    name = models.CharField(max_length=100, default='', blank=True)
    description = models.TextField(default='', blank=True)
    sprint = models.ForeignKey('Sprint', blank=True, null=True)
    state = models.SmallIntegerField(choices=TaskState.get_choices(), default=TaskState.TODO)
    order = models.SmallIntegerField(default=0)
    assigned = models.ForeignKey(settings.AUTH_USER_MODEL, null=True, blank=True)
    started = models.DateField(null=True, blank=True)
    due = models.DateField(null=True, blank=True)
    completed = models.DateField(null=True, blank=True)

    def __str__(self):
        return self.name

    @property
    def status_display(self):
        return TaskState.values[self.state]

    class Meta:
        db_table = 'task'
