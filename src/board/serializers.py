# coding:utf-8
from datetime import date
from django.conf import settings
from django.contrib.auth import get_user_model
from django.core.signing import TimestampSigner
from rest_framework import serializers
from rest_framework.reverse import reverse

from .models import Sprint, Task, TaskState


User = get_user_model()


class SprintSerializer(serializers.ModelSerializer):
    """
    """
    links = serializers.SerializerMethodField()

    def get_links(self, obj):
        request = self.context['request']
        signer = TimestampSigner(settings.WATERCOOLER_SECRET)
        channel = signer.sign(obj.pk)
        return {
            'self': reverse(
                'sprint-detail', kwargs={'pk': obj.pk}, request=request),
            'tasks': reverse(
                'task-list', request=request) + '?sprint={}'.format(obj.pk),
            'channel': '{proto}://{server}/socket?channel={channel}'.format(
                proto='wss' if settings.WATERCOOLER_SECURE else 'ws',
                server=settings.WATERCOOLER_SERVER,
                channel=channel
            )
        }

    def validate_end(self, value):
        if value < date.today():
            raise serializers.ValidationError(
                'Дата окончания спринта не может быть раньше текущей')
        return value

    class Meta:
        model = Sprint
        fields = ('id', 'name', 'description', 'end', 'links')


class TaskSerializer(serializers.ModelSerializer):
    """
    """
    assigned = serializers.SlugRelatedField(
        slug_field=User.USERNAME_FIELD, required=False,
        queryset=User.objects.all())
    status_display = serializers.SerializerMethodField()
    links = serializers.SerializerMethodField()

    def get_status_display(self, obj):
        return obj.status_display

    def get_links(self, obj):
        request = self.context['request']
        links = {
            'self': reverse(
                'task-detail', kwargs={'pk': obj.pk}, request=request),
            'sprint': None,
            'assigned': None
        }
        if obj.sprint_id:
            links['sprint'] = reverse(
                'sprint-detail', kwargs={'pk': obj.sprint_id}, request=request)
        if obj.assigned:
            links['assigned'] = reverse(
                'user-detail',
                kwargs={User.USERNAME_FIELD: obj.assigned},
                request=request)
        return links

    def validate_sprint(self, value):
        if self.instance and self.instance.pk:
            if value != self.instance.sprint:
                if self.instance.state == TaskState.DONE:
                    raise serializers.ValidationError(
                        u'Нельзя поменять спринт для выполненной задачи')
                if value and value.end < date.today():
                    raise serializers.ValidationError(
                        'Нельзя добавить задачу в завершенный спринт')
        else:
            if value and value.end < date.today():
                raise serializers.ValidationError(
                    'Нельзя добавить задачу в завершенный спринт')
        return value

    def validate(self, attrs):
        sprint = attrs.get('sprint')
        status = attrs.get('state', TaskState.TODO)
        started = attrs.get('started')
        completed = attrs.get('completed')

        if not sprint and status != TaskState.TODO:
            raise serializers.ValidationError(
                'Задачи бэклога должны иметь статус "{}"'.format(
                    TaskState.values[TaskState.TODO]))
        if started and status == TaskState.TODO:
            raise serializers.ValidationError(
                'Дата начала не может быть установлена для неначатых задач')
        if completed and status != TaskState.DONE:
            raise serializers.ValidationError(
                'Дата завершения не может быть установлена '
                'для незавершенных задач')

        return attrs

    class Meta:
        model = Task
        fields = ('id', 'name', 'description', 'sprint', 'state',
                  'status_display', 'order', 'assigned', 'started', 'due',
                  'completed', 'links')

class UserSerializer(serializers.ModelSerializer):
    """
    """
    full_name = serializers.CharField(source='get_full_name', read_only=True)
    links = serializers.SerializerMethodField()

    def get_links(self, obj):
        request = self.context['request']
        username = obj.get_username()
        return {
            'self': reverse(
                'user-detail',
                kwargs={User.USERNAME_FIELD: username},
                request=request),
            'tasks': '{}?assigned={}'.format(
                reverse('task-list', request=request), username)
        }

    class Meta:
        model = User
        fields = ('id', User.USERNAME_FIELD, 'full_name', 'is_active', 'links')
