# coding:utf-8
from rest_framework.routers import DefaultRouter

from . import views


board_router = DefaultRouter(trailing_slash=False)
board_router.register(r'sprints', views.SprintViewSet)
board_router.register(r'tasks', views.TaskViewSet)
board_router.register(r'users', views.UserViewSet)
